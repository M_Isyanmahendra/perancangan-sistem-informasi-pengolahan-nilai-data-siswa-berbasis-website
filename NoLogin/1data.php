<?php

require "../php/crud/koneksi.php";

$data = mysqli_query($koneksi, "SELECT * FROM tbl_data");

require '../php/crud/cariData.php';
require '../php/crud/removedata.php';
require '../php/crud/updateData.php';

if (isset($_GET['btn-cari-data'])) {
    $data = cariData();
} else if (isset($_GET['hapus'])) {
    hapus();
} else if (isset($_GET['update'])) {
    perbarui();
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="Muhammad Isya Noor Mahendra" content="Pengelolaan nilai data siswa berbasis website">
    <title>SMAN 6 Madiun</title>
    <link rel="stylesheet" href="../CSS/style-data.css">
    <script src="https://kit.fontawesome.com/0939618a2a.js" crossorigin="anonymous"></script>
    <script src="highcharts/highcharts.js"></script>
    <script src="highcharts/modules/exporting.js"></script>
    <script src="highcharts/modules/export-data.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

</head>
<body> 

    <!-- Navbar -->
    <div class="div-navbar">
        <div class="wrapper-nav">
        <div class="image">
            <img src="../Gambar/logo Sman 6.png" alt="" width="90" height="90" >
        </div>
        <!-- hamburger -->
        <div class="toggle">
            <i class="ham-btn fa-solid fa-bars"></i>
        </div>
        <!-- hamburger -->
        </div>

        <ul class="ul-navbar">
            <li class="li-navbar">
                <a href="1index.php" class="a-navbar">HOME</a>
            </li>
            <li class="li-navbar">
                <a href="1about.php" class="a-navbar">ABOUT</a>
            </li>
            <li class="li-navbar">
                <a href="1data.php" class="a-navbar">DATA</a>
            </li>
            <li class="li-navbar">
                <a href="../php/login.php" class="a-navbar">LOG IN</a>
            </li>
        </ul>
    </div>
    

    
<script src="../php/menu.js"></script>





<!-- content data siswa  -->
<div class="tabel">
<h1 class="main-title">Sistem Manajemen Penilaian Data siswa :</h1><br>
<!-- <form class="formsearch">
    <h2>Search untuk mencari data : </h2><br>
    <input type="text" name="search" placeholder="Search.."> -->

    <!-- <div class=""> 
    <a href="./inputdata.php"><button>Tambah Data</button></a>
    </div>-->

    </form>
<br>
        <section class="main-tabel">
            <table>
                <thead>
                <tr>
                    <th>Kelas</th>
                    <th>Nama</th>
                    <th>NPSN</th>
                    <th>Jurusan</th>
                    <th>Rata-Rata UTS</th>
                    <th>Rata-Rata UAS</th>
                    <th>Akumulasi Nilai</th>
                </tr>
                </thead>
                <tbody>

                <?php foreach ($data as $kelas => $dt): ?>
                        <tr>

                            <td><?php echo $dt['kelas'] ?></td>
                            <td><?php echo $dt['nama'] ?></td>
                            <td><?php echo $dt['npsn'] ?></td>
                            <td><?php echo $dt['jurusan'] ?></td>
                            <td><?php echo $dt['uts'] ?></td>
                            <td><?php echo $dt['uas'] ?></td>
                            <td><?php echo $dt['akumulasi'] ?></td>
                        </tr>
                <?php endforeach;?>

                </tbody>
            </table>
        </section>
    </div>

<!-- footer -->
    <div class="footer">
        <div class="row">
            <div class="footer-col">
                <h4> Ikuti Kami</h4>
                <div class="social-links">
                    <a href="https://api.whatsapp.com/send/?phone=6289603050983&text&type=phone_number&app_absent=0"><i class="fa-brands fa-whatsapp"></i></a>
                    <a href="https://instagram.com/sman6madiun?igshid=YmMyMTA2M2Y="><i class="fa-brands fa-instagram"></i></a>
                    <a href="https://m.facebook.com/profile.php?id=1437001096607357"><i class="fa-brands fa-facebook"></i></a>
                    <a href="https://twitter.com/smasix_madiun"><i class="fa-brands fa-twitter"></i></a>
                </div>
                </div>
                <div class="footer-col">
                    <h4><b>Layanan kami</b></h4>
                    <ul class="ul">
                        <li><a href="https://instagram.com/sman6madiun?igshid=YmMyMTA2M2Y=">Bantuan</a></li><br>
                        <li><a href="https://api.whatsapp.com/send/?phone=6289603050983&text&type=phone_number&app_absent=0">Kontak</a></li><br>
                        <li><a href="about.php">Sejarah</a></li>
                    </ul>
                </div>
                <div class="footer-col">
                    <h4><b>Alamat Kami</b></h4>
                    <ul class="ul">
                        <li><a href="https://goo.gl/maps/3LdaeGX3r7i5RDko6">Jl. Suhud Nosingo No. 1 Madiun.<br> Kodepos: 63132</a></li><br>
                        <li><a href="https://goo.gl/maps/3LdaeGX3r7i5RDko6">©Copyright 2022 | Oleh M_Isya_N_M</a></li><br>
                    </ul>
                </div>
            </div>
         </div>
    </div>



    <script src="menu.js"></script>

</body>
</html>