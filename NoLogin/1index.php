
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="Muhammad Isya Noor Mahendra" content="Pengelolaan nilai data siswa berbasis website">
    <title>Halaman Home SMAN 6 Madiun</title>
    <link rel="stylesheet" href="../CSS/style-home.css">
    <script src="https://kit.fontawesome.com/0939618a2a.js" crossorigin="anonymous"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>

<body>
    <!-- Navbar -->
    <div class="div-navbar">
        <div class="wrapper-nav">
        <div class="image">
            <img src="../Gambar/logo Sman 6.png" alt="" width="90" height="90" >
        </div>
        <!-- hamburger -->
        <div class="toggle">
            <i class="ham-btn fa-solid fa-bars"></i>
        </div>
        <!-- hamburger -->
        </div>

        <ul class="ul-navbar">
            <li class="li-navbar">
                <a href="1index.php" class="a-navbar">HOME</a>
            </li>
            <li class="li-navbar">
                <a href="1about.php" class="a-navbar">ABOUT</a>
            </li>
            <li class="li-navbar">
                <a href="1data.php" class="a-navbar">DATA</a>
            </li>
            <li class="li-navbar">
                <a href="../php/login.php" class="a-navbar">LOG IN</a>
            </li>
        </ul>
    </div>

    <!-- Content 1 -->
    <div class="content1">
        <div class="">
            <p class="font-pembukaan"> 
                Selamat Datang <br>
                Di Website Sistem Informasi <br>
                SMA N 6 Madiun
            </p>
        </div>
    </div>



        <!-- CONTENT 2 -->
        <div class="container">
            <div>
                <img src="../Gambar/kepala sekolah.png" width="250" height="250" />
                <div class="content-font1">

                    <h1>Sambutan Kepala Sekolah</h1><br>
                    <p>Assalamu'alaikum wr.wb. <br>

                        Puji syukur kepada Alloh SWT, Tuhan Yang Maha Esa yang telah memberikan rahmat dan anugerahNya sehingga website SMAN 6 Madiun ini dapat terbit. Salah satu tujuan dari website ini adalah untuk menjawab akan setiap kebutuhan informasi dengan memanfaatkan sarana teknologi informasi yang ada. Kami sadar sepenuhnya dalam rangka memajukan pendidikan di era berkembangnya Teknologi Informasi yang begitu pesat, sangat diperlukan berbagai sarana prasarana yang kondusif, kebutuhan berbagai informasi siswa, guru, orangtua maupun masyarakat, sehingga kami berusaha mewujudkan hal tersebut semaksimal mungkin. Semoga dengan adanya website ini dapat membantu dan bermanfaat, terutama informasi yang berhubungan dengan pendidikan, ilmu pengetahuan dan informasi seputar SMAN 6 Madiun.
                        
                        Besar harapan kami, sarana ini dapat memberi manfaat bagi semua pihak yang ada dilingkup pendidikan dan pemerhati pendidikan secara khusus bagi SMAN 6 Madiun.
                        
                        Akhirnya kami mengharapkan masukan dari berbagai pihak untuk website ini agar kami terus belajar dan meng-update diri, sehingga tampilan, isi dan mutu website akan terus berkembang dan lebih baik nantinya. Terima kasih atas kerjasamanya, maju terus untuk mencapai SMAN 6 Madiun yang lebih baik lagi.
                        <br>
                        
                        Wassalamu'alaikum wr.wb.
                        </p><br>

                    <h1>Program Sekolah</h1><br>
                    <p>Sesuai visi dan misi sekolah bahwa SMA Negeri 6 Madiun selalu berupaya untuk meningkatkan
                        prosentase siswa yang melanjutkan ke jenjang lebih tinggi utamanya Perguruan Tinggi Negeri dan
                        Sekolah Kedinasan.</p> <br>

                    <h1>Tujuan pendidikan sekolah</h1><br>
                    <p>Pendidikan nasional berfungsi mengembangkan kemampuan dan membentuk watak serta peradaban bangsa
                        yang bermartabat dalam rangka mencerdaskan kehidupan bangsa, bertujuan untuk berkembangnya
                        potensi peserta didik agar menjadi manusia yang beriman dan bertakwa kepada Tuhan Yang Maha Esa,
                        berakhlak mulia, sehat, berilmu,...</p>

                </div>
            </div>
        </div>











        <div class="footer">
            <div class="row">
                <div class="footer-col">
                    <h4> <b>Ikuti Kami</b></h4>
                    <div class="social-links">
                        <a href="https://api.whatsapp.com/send/?phone=6289603050983&text&type=phone_number&app_absent=0"><i class="fa-brands fa-whatsapp"></i></a>
                        <a href="https://instagram.com/sman6madiun?igshid=YmMyMTA2M2Y="><i class="fa-brands fa-instagram"></i></a>
                        <a href="https://m.facebook.com/profile.php?id=1437001096607357"><i class="fa-brands fa-facebook"></i></a>
                        <a href="https://twitter.com/smasix_madiun"><i class="fa-brands fa-twitter"></i></a>
                    </div>
                    </div>
                    <div class="footer-col">
                        <h4 ><b>Layanan kami</b></h4>
                        <ul class="ul">
                            <li><a href="https://instagram.com/sman6madiun?igshid=YmMyMTA2M2Y=">Bantuan</a></li><br>
                            <li><a href="https://api.whatsapp.com/send/?phone=6289603050983&text&type=phone_number&app_absent=0">Kontak</a></li><br>
                            <li><a href="about.php">Sejarah</a></li>
                        </ul>
                    </div>
                    <div class="footer-col">
                        <h4><b>Alamat Kami</b></h4>
                        <ul class="ul">
                            <li><a href="https://goo.gl/maps/3LdaeGX3r7i5RDko6">Jl. Suhud Nosingo No. 1 Madiun.<br> Kodepos: 63132</a></li><br>
                            <li><a href="https://goo.gl/maps/3LdaeGX3r7i5RDko6">©Copyright 2022 | Oleh M_Isya_N_M</a></li><br>
                        </ul>
                    </div>
                </div>
             </div>
        </div>
    
    
    
        <script src="menu.js"></script>





</body>

</html>