const ham = document.querySelector('.toggle');
const icons = document.querySelector('i.ham-btn');
const lists = document.querySelector('.ul-navbar');

ham.addEventListener('click', function () {
    icons.classList.toggle('fa-xmark');
    lists.classList.toggle('active')
});

function openNav() {
  document.getElementById("myNav").style.height = "100%";
}

function closeNav() {
  document.getElementById("myNav").style.height = "0%";
}