
// coba diagram 
window.onload = function() {

  var options = {
    exportEnabled: true,
    animationEnabled: true,
    title:{
      text: "Informasi Rata Rata Rumah Siswa"
    },
    legend:{
      horizontalAlign: "right",
      verticalAlign: "center"
    },
    data: [{
      type: "pie",
      showInLegend: true,
      toolTipContent: "<b>{name}</b>: {y} (#percent%)",
      indexLabel: "{name}",
      legendText: "{name} (#percent%)",
      indexLabelPlacement: "inside",
      dataPoints: [
        { y: 4566.4, name: "Kejuron" },
        { y: 2599.2, name: "Kuncen" },
        { y: 1231.2, name: "Taman" },
        { y: 1368, name: "Josenan" },
        { y: 2684, name: "Mojorejo"},
        { y: 1231.2, name: "Pandean" }
      ]
    }]
  };
  $("#chartContainer").CanvasJSChart(options);
  
  }