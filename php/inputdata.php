<?php
require './crud/cariData.php';
require './crud/removedata.php';
require './crud/updatedata.php';

if (isset($_GET['btn-cari-data'])) {
    $data = cariData();
} else if (isset($_GET['hapus'])) {
    hapus();
} else if (isset($_GET['update'])) {
    perbarui();
}

?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Sistem Manajemen Data</title>
    <link rel="stylesheet" type="text/css" href="../CSS/inputdata.css">
    <script src="https://kit.fontawesome.com/0939618a2a.js" crossorigin="anonymous"></script>

</head>

<H1 class="judul-login">Silahkan Masukkan Data <br> Penilaian Siswa<br>SMA 6 Madiun</H1>

<body class="body-gedung">

    <main>

        <form action="./crud/tambahData.php" method="POST">
            <div class="kelas">
                <label for="kelas">Masukkan kelas</label>
                <input type="text" name="kelas" id="kelas" placeholder="Masukkan Kelas">
            </div>
            <div class="nama">
                <label for="nama">Masukkan Nama</label>
                <input type="text" name="nama" id="nama" placeholder="Masukkan Nama">
            </div>
            <div class="npsn">
                <label for="npsn">Masukkan NPSN</label>
                <input type="text" name="npsn" id="npsn" placeholder="Masukkan NPSN">
            </div>
            <div class="jurusan">
                <label for="jurusan">Masukkan Jurusan</label>
                <input type="text" name="jurusan" id="jurusan" placeholder="Masukkan Jurusan">
            </div>
            <div class="uts">
                <label for="uts">Masukkan Rata-Rata UTS</label>
                <input type="text" name="uts" id="uts" placeholder="Masukkan Nilai UTS">
            </div>
            <div class="uas">
                <label for="uas">Masukkan Rata-Rata UAS</label>
                <input type="text" name="uas" id="uas" placeholder="Masukkan Nilai UAS">
            </div>
            <div class="submit-nilai">
                <button type="submit">Tambah Data</button>
            </div>
            <div class="submit-nilai">
            <button type="submit">
                <p><a href="../php/data.php" style="color: aliceblue;">Kembali ?</a></p>
            </div>
            
        </form>

        <form action="" method="GET">
            <div class="submit-nilai">

                <input type="search" name="cari-data" id="cari-data" placeholder="Masukkan Nama Untuk Mencari Data">
                <button type="submit" name="btn-cari-data">Cari</button>
            </div>

            <?php if (isset($_GET['btn-cari-data'])): ?>
            <div class="kelas">
                <label for="kelas">Masukkan Kelas</label>
                <input type="text" name="kelas" id="kelas" value="<?php echo $data['kelas'] ?>">
            </div>
            <div class="nama">
                <label for="nama">Masukkan Nama</label>
                <input type="text" name="nama" id="nama" value="<?php echo $data['nama'] ?>">
            </div>
            <div class="npsn">
                <label for="npsn">Masukkan NPSN</label>
                <input type="text" name="npsn" id="npsn" value="<?php echo $data['npsn'] ?>">
            </div>
            <div class="jurusan">
                <label for="jurusan">Masukkan Jurusan</label>
                <input type="text" name="jurusan" id="jurusan" value="<?php echo $data['jurusan'] ?>">
            </div>
            <div class="uts">
                <label for="uts">Masukkan Nilai Rata Rata UTS</label>
                <input type="text" name="uts" id="uts" value="<?php echo $data['uts'] ?>">
            </div>
            <div class="uas">
                <label for="uas">Masukkan Nilai Rata Rata UAS</label>
                <input type="text" name="uas" id="uas" value="<?php echo $data['uas'] ?>">
            </div>
            <div class="submit-nilai">
                <button type="submit" name="hapus">Hapus</button>
                <button type="submit" name="update">Update</button>
            </div>
            <?php endif;?>
            </form>
        </form>

    </main>
</body>

</html>
