<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="Muhammad Isya Noor Mahendra" content="Pengelolaan nilai data siswa berbasis website">
    <title>SMAN 6 Madiun</title>
    <link rel="stylesheet" href="../CSS/style-about.css">
    <script src="https://kit.fontawesome.com/0939618a2a.js" crossorigin="anonymous"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body> 

<!-- Navbar -->
<div class="div-navbar">
    <div class="wrapper-nav">
    <div class="image">
        <img src="../Gambar/logo Sman 6.png" alt="" width="90" height="90" >
    </div>
    <!-- hamburger -->
    <div class="toggle">
        <i class="ham-btn fa-solid fa-bars"></i>
    </div>
    <!-- hamburger -->
    </div>
    <ul class="ul-navbar">
        <li class="li-navbar">
            <a href="index.php" class="a-navbar">HOME</a>
        </li>
        <li class="li-navbar">
            <a href="about.php" class="a-navbar">ABOUT</a>
        </li>
        <li class="li-navbar">
            <a href="data.php" class="a-navbar">DATA</a>
        </li>
        <li class="li-navbar">
            <a href="logout.php" class="a-navbar">LOG OUT</a>
        </li>
    </ul>
</div>

    <script src="menu.js"></script>
   
    <!-- Content 1 -->
    
    <div class="gambar">
    <h1 class="judul-about">SMA 6 Madiun  </h1><br>
    
        <img src="../Gambar/gambar_smasix1.png" alt="" width="400">
    </div>
    <div class="font-content1"><br>
        <h1 class="judul1">Sejarah</h1>
        <br>
        <p>
            Pada awal berdirinya, sekolah ini bernama Sekolah Menengah Atas Negeri (SMAN) 3 karena sekolah ini merupakan pecahan dari SMAN 3 Madiun. Kemudian, pada tanggal 26 Oktober 1995, Pemerintah melalui Keputusan Menteri Pendidikan dan Kebudayaan Republik Indonesia Nomor: 0315/O/1995 tentang “PEMBUKAAN DAN PENEGERIAN SEKOLAH TAHUN PELAJARAN 1994/1995” mengubah nama dan status SMA menjadi SMA Negeri 6 Madiun yang pada saat itu masih beralamat di Jl. Agus Salim Madiun

            Kemudian pada saat dipimpin oleh Drs Hendrijanto sebagai kepala sekolah pada tahun pelajaran 2009/2010, sekolah ini pindah tempat ke Jl. Suhud Nosingo, Madiun. Gedung yang digunakan di jalan itu adalah bekas gedung SMAN 3 dan masih digunakan hingga sekarang.
            
            SMA Negeri 6 Madiun merupakan satuan pendidikan yang telah terakreditasi A. Untuk meningkatkan mutu pendidikan dan daya saing lulusan SMA Negeri 6 Madiun, maka dibutuhkan program-program yang dapat mendorong terciptanya ekosistem pendidikan yang baik yang lebih dari SNP baik dari aspek siswa, tenaga pendidik dan kependidikan, sarana dan prasarana, serta manajemen sekolah. Sejak tahun 2016 SMA Negeri 6 Madiun telah mengimplementasikan kurikulum 2013 dan dikembangkan serta disesuaikan dengan karakteristik daerah, satuan pendidikan, dan peserta didik.
            </p>
    </div><br>


    <div class="footer">
        <div class="row">
            <div class="footer-col">
                <h4><b>Ikuti Kami</b></h4>
                <div class="social-links">
                    <a href="https://api.whatsapp.com/send/?phone=6289603050983&text&type=phone_number&app_absent=0"><i class="fa-brands fa-whatsapp"></i></a>
                    <a href="https://instagram.com/sman6madiun?igshid=YmMyMTA2M2Y="><i class="fa-brands fa-instagram"></i></a>
                    <a href="https://m.facebook.com/profile.php?id=1437001096607357"><i class="fa-brands fa-facebook"></i></a>
                    <a href="https://twitter.com/smasix_madiun"><i class="fa-brands fa-twitter"></i></a>
                </div>
                </div>
                <div class="footer-col">
                    <h4><b>Layanan kami</b></h4>
                    <ul class="ul">
                        <li><a href="https://instagram.com/sman6madiun?igshid=YmMyMTA2M2Y=">Bantuan</a></li><br>
                        <li><a href="https://api.whatsapp.com/send/?phone=6289603050983&text&type=phone_number&app_absent=0">Kontak</a></li><br>
                        <li><a href="about.php">Sejarah</a></li>
                    </ul>
                </div>
                <div class="footer-col">
                    <h4><b>Alamat Kami</b></h4>
                    <ul class="ul">
                        <li><a href="https://goo.gl/maps/3LdaeGX3r7i5RDko6">Jl. Suhud Nosingo No. 1 Madiun.<br> Kodepos: 63132</a></li><br>
                        <li><a href="https://goo.gl/maps/3LdaeGX3r7i5RDko6">©Copyright 2022 | Oleh M_Isya_N_M</a></li><br>
                    </ul>
                </div>
            </div>
         </div>
    </div>

    

</body>
</html>